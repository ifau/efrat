//
//  DataManager.swift
//  efrat
//
//  Created by ifau on 12/12/15.
//  Copyright © 2015 tabus. All rights reserved.
//

import UIKit

let catalogWasSuccessfullyUpdatedNotification = "catalogWasSuccessfullyUpdatedNotification"
let catalogWasFailedUpdateNotification = "catalogWasFailedUpdateNotification"

let orderWasSuccessfullySendedNotification = "orderWasSuccessfullySendedNotification"
let orderWasFailedSendNotification = "orderWasFailedSendNotification"

class DataManager: NSObject
{
    static let sharedInstance = DataManager()
    
    var catalogItems : [CatalogItemModel] = []
    var orderDictionary = Dictionary<String, Int>()
    
    var canOrder : Bool
    {
        get
        {
            return orderDictionary.keys.count > 0
        }
    }
    
    var orderNumbersColumn : String
    {
        get
        {
            var numbers = "№\n"
            for (index, _) in orderDictionary.enumerate() { numbers += "\(index + 1).\n" }
            return numbers
        }
    }
    
    var orderTitlesColumn : String
    {
        get
        {
            var titles = "Название\n"
            for (key, _) in orderDictionary
            {
                let item = catalogItems.filter{ $0.iD == key }.first!
                titles += "\(item.name), \(item.volume)л.\n"
            }
            return titles
        }
    }
    
    var orderCountsColumn : String
    {
        get
        {
            var counts = "К-во, шт.\n"
            for (_, value) in orderDictionary { counts += "\(value)\n" }
            return counts
        }
    }
    
    var orderPricesColumn : String
    {
        get
        {
            var prices = "Цена, ₽\n"
            for (key, _) in orderDictionary
            {
                let item = catalogItems.filter{ $0.iD == key }.first!
                prices += "\(Int(item.price))\n"
            }
            return prices
        }
    }
    
    var presum : Int
    {
        get
        {
            var sum = 0
            for (key, value) in orderDictionary
            {
                let item = catalogItems.filter{ $0.iD == key }.first!
                sum += (Int(item.price) * value)
            }
            return sum
        }
    }
    
    var maxempty : Int
    {
        get
        {
            var empty = 0
            for (_, value) in orderDictionary { empty += value }
            return empty
        }
    }
    
    var empty : Int = 0
    var totalsum : Int = 0
    
    var clientName : String?
    var clientPhone : String?
    var clientCity : String = "Москва"
    var clientStreet : String?
    var clientHouse : String?
    var clientApartament : String?
    var clientComment : String?
    
    
    func updateCatalog()
    {
        Requester.sharedInstance.performRequest(path: "Catalog", data: nil) {[unowned self] (dic: NSDictionary?) -> () in
            
            if dic != nil
            {
                let response = ResponseModel(fromDictionary: dic!)
                self.catalogItems.removeAll()
                self.catalogItems = response.items
                NSNotificationCenter.defaultCenter().postNotificationName(catalogWasSuccessfullyUpdatedNotification, object: nil)
            }
            else
            {
                NSNotificationCenter.defaultCenter().postNotificationName(catalogWasFailedUpdateNotification, object: nil)
            }
        }
    }
    
    func sendOrder()
    {
        var ordedArray : [Dictionary<String, AnyObject>] = []
        for (key, value) in orderDictionary
        {
            var dic = Dictionary<String, AnyObject>()
            dic["ID"] = key
            dic["Count"] = value
            ordedArray.append(dic)
        }
        
        let address = "Город:\(clientCity)\nУлица:\(clientStreet ?? "-")\nДом:\(clientHouse ?? "-")\nКвартира:\(clientApartament ?? "-")"
        let dictionary =
        [
            "Cart" : ordedArray,
            "Name" : clientName ?? NSNull(),
            "Phone" : clientPhone ?? NSNull(),
            "Address" :address,
            "Description" : clientComment ?? NSNull(),
            "TotalSum" : NSNumber(integer: totalsum),
            "PreSum" : NSNumber(integer: presum),
            "EmptyCrate" : NSNumber(integer: empty)
        ]
        
        do
        {
            let data = try NSJSONSerialization.dataWithJSONObject(dictionary, options: NSJSONWritingOptions(rawValue: 0))
            
            Requester.sharedInstance.performRequest(path: "Order", data: data, completion: { [unowned self] (dic: NSDictionary?) -> () in
                
                if dic != nil
                {
                    self.orderDictionary.removeAll()
                    NSNotificationCenter.defaultCenter().postNotificationName(orderWasSuccessfullySendedNotification, object: nil)
                }
                else
                {
                    NSNotificationCenter.defaultCenter().postNotificationName(orderWasFailedSendNotification, object: nil)
                }
            })
        }
        catch
        {
            NSNotificationCenter.defaultCenter().postNotificationName(orderWasFailedSendNotification, object: nil)
        }
    }
}
