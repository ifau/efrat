//
//  CatalogItemCell.swift
//  efrat
//
//  Created by ifau on 10/12/15.
//  Copyright © 2015 tabus. All rights reserved.
//

import UIKit
import Foundation

class CatalogItemCell: UITableViewCell
{
    @IBOutlet var pictureImageView: UIImageView!
    @IBOutlet var descriptionLabel: UILabel!

    @IBOutlet var countStepper: SnappingStepper!
    @IBOutlet var addtocartButton: UIButton!
    
    required init?(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)
    }
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        //backgroundColor = UIColor(red: 40/255.0, green: 185/255.0, blue: 228/255.0, alpha: 1)
        
        descriptionLabel.textColor = UIColor.whiteColor()
        descriptionLabel.font = UIFont.systemFontOfSize(15)
        
        countStepper.continuous = true
        countStepper.autorepeat = true
        countStepper.wraps = false
        countStepper.minimumValue = 0
        countStepper.stepValue = 1
        countStepper.maximumValue = 1000
        countStepper.thumbText = nil
        
        countStepper.symbolFont = UIFont.systemFontOfSize(20)
        countStepper.symbolFontColor = UIColor.blackColor()
        countStepper.backgroundColor = UIColor(red: 0.95, green: 0.95, blue: 0.95, alpha: 1.0)
        countStepper.thumbFont = UIFont.boldSystemFontOfSize(20)
        countStepper.thumbBackgroundColor = UIColor.whiteColor()
        countStepper.thumbTextColor = UIColor.blackColor()
        
        addtocartButton.backgroundColor = UIColor.whiteColor()
        addtocartButton.setTitle("", forState: UIControlState.Normal)
        addtocartButton.setImage(UIImage(named: "IconCartAdd"), forState: UIControlState.Normal)
    }
}
