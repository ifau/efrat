//
//  CatalogItemModel.swift
//  efrat
//
//  Created by ifau on 10/12/15.
//  Copyright © 2015 tabus. All rights reserved.
//

import UIKit

class CatalogItemModel: NSObject
{
    var iD : String!
    var name : String!
    var picture : String!
    var price : Float!
    var volume : Int!

    init(fromDictionary dictionary: NSDictionary)
    {
        iD = dictionary["ID"] as? String
        name = dictionary["Name"] as? String
        picture = dictionary["Picture"] as? String
        price = dictionary["Price"] as? Float
        volume = dictionary["Volume"] as? Int
    }
    
    var aDescription : String
    {
        get
        {
            return "\(name), \(volume) литров\n\nЦена: \(price)₽"
        }
    }
}

class ResponseModel: NSObject
{
    var items : [CatalogItemModel]!
    var message : String!

    init(fromDictionary dictionary: NSDictionary)
    {
        items = [CatalogItemModel]()
        if let itemsArray = dictionary["Item"] as? [NSDictionary]
        {
            for dic in itemsArray
            {
                let item = CatalogItemModel(fromDictionary: dic)
                items.append(item)
            }
        }
        message = dictionary["Message"] as? String
    }
}