//
//  OrderViewController.swift
//  efrat
//
//  Created by ifau on 11/12/15.
//  Copyright © 2015 tabus. All rights reserved.
//

import UIKit

class OrderViewController: UIViewController
{
    @IBOutlet var orderButton: UIBarButtonItem!
    
    @IBOutlet private var numberLabel: UILabel!
    @IBOutlet private var titleLabel: UILabel!
    @IBOutlet private var countLabel: UILabel!
    @IBOutlet private var priceLabel: UILabel!

    @IBOutlet private var emptycountStepper: SnappingStepper!
    
    @IBOutlet private var presumLabel: UILabel!
    @IBOutlet private var depositLabel: UILabel!
    @IBOutlet private var totalsumLabel: UILabel!
    
    var emptycount: Int = 0
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        orderButton.enabled = DataManager.sharedInstance.canOrder
        
        numberLabel.text = DataManager.sharedInstance.orderNumbersColumn
        titleLabel.text = DataManager.sharedInstance.orderTitlesColumn
        countLabel.text = DataManager.sharedInstance.orderCountsColumn
        priceLabel.text = DataManager.sharedInstance.orderPricesColumn
        
        presumLabel.text = "\(DataManager.sharedInstance.presum)₽"
        
        emptycountStepper.continuous = true
        emptycountStepper.autorepeat = true
        emptycountStepper.wraps = false
        emptycountStepper.minimumValue = 0
        emptycountStepper.stepValue = 1
        emptycountStepper.maximumValue = 1000
        emptycountStepper.thumbText = nil
        emptycountStepper.symbolFont = UIFont.systemFontOfSize(20)
        emptycountStepper.symbolFontColor = UIColor.blackColor()
        emptycountStepper.backgroundColor = UIColor(red: 0.95, green: 0.95, blue: 0.95, alpha: 1.0)
        emptycountStepper.thumbFont = UIFont.boldSystemFontOfSize(20)
        emptycountStepper.thumbBackgroundColor = UIColor.whiteColor()
        emptycountStepper.thumbTextColor = UIColor.blackColor()
        emptycountStepper.addTarget(self, action: "stepperPressed:", forControlEvents: .ValueChanged)
        emptycountStepper.value = 0
        
        updateLabels()
    }
    
    @IBAction func closeButtonPressed(sender: AnyObject)
    {
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    func stepperPressed(sender: AnyObject)
    {
        updateLabels()
    }
    
    func updateLabels()
    {
        emptycount = Int(emptycountStepper.value)
        if emptycount > DataManager.sharedInstance.maxempty
        {
            emptycount = DataManager.sharedInstance.maxempty
        }
        
        let deposit = (DataManager.sharedInstance.maxempty - emptycount) * 210
        let totalsum = DataManager.sharedInstance.presum + deposit
        
        depositLabel.text = "\(deposit)₽"
        totalsumLabel.text = "\(totalsum)₽"
        
        DataManager.sharedInstance.empty = emptycount
        DataManager.sharedInstance.totalsum = totalsum
    }
}
