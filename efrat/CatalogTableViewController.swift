//
//  CatalogTableViewController.swift
//  efrat
//
//  Created by ifau on 10/12/15.
//  Copyright © 2015 tabus. All rights reserved.
//

import UIKit
import Kingfisher

class CatalogTableViewController: UIViewController, UITableViewDelegate, UITableViewDataSource
{
    @IBOutlet var tableView: UITableView!
    var orderButton: ENMBadgedBarButtonItem!
    
    var refreshControl : UIRefreshControl!
    var stepperItems : Dictionary<String, Int>!
    
    override func viewDidLoad()
    {
        tableView.delegate = self
        tableView.dataSource = self
        
        refreshControl = UIRefreshControl()
        refreshControl.tintColor = UIColor.whiteColor()
        refreshControl.addTarget(self, action: "updateCatalog", forControlEvents: .ValueChanged)
        tableView.addSubview(refreshControl)
        
        stepperItems = Dictionary<String, Int>()
        setUpRightBarButton()
        setUpLeftBarButton()
        
        updateCatalog()
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "catalogDidUpdated", name: catalogWasSuccessfullyUpdatedNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "catalogDidFailedUpdate", name: catalogWasFailedUpdateNotification, object: nil)
    }
    
    override func viewWillAppear(animated: Bool)
    {
        super.viewWillAppear(animated)
        updateBadge()
    }
    
    deinit
    {
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    func setUpRightBarButton()
    {
        let image = UIImage(named: "IconCart")!.imageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate)
        let button = UIButton(type: .Custom)
        
        button.tintColor = UIColor.whiteColor()
        button.frame = CGRectMake(0.0, 0.0, image.size.width, image.size.height)
        button.setBackgroundImage(image, forState: UIControlState.Normal)
        button.addTarget(self, action: "orderButtonPressed:", forControlEvents: UIControlEvents.TouchUpInside)

        orderButton = ENMBadgedBarButtonItem(customView: button, value: "")
        orderButton.badgeBackgroundColor = UIColor.redColor()
        orderButton.badgeTextColor = UIColor.whiteColor()
        navigationItem.rightBarButtonItem = orderButton
    }
    
    func setUpLeftBarButton()
    {
        let image = UIImage(named: "IconInfo")!.imageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate)
        let button = UIButton(type: .Custom)
        
        button.tintColor = UIColor(red: 150/255.0, green: 225/255.0, blue: 247/255.0, alpha: 1.0)
        button.frame = CGRectMake(0.0, 0.0, image.size.width, image.size.height)
        button.setBackgroundImage(image, forState: UIControlState.Normal)
        button.addTarget(self, action: "aboutTabusPressed:", forControlEvents: UIControlEvents.TouchUpInside)
        
        let tabusButton = ENMBadgedBarButtonItem(customView: button, value: "")
        tabusButton.badgeBackgroundColor = UIColor.redColor()
        tabusButton.badgeTextColor = UIColor.whiteColor()
        navigationItem.leftBarButtonItem = tabusButton
    }
    
    // MARK: - logic
    
    func updateCatalog()
    {
        DataManager.sharedInstance.updateCatalog()
    }
    
    func catalogDidUpdated()
    {
        self.refreshControl.endRefreshing()
        self.tableView.reloadData()
        updateBadge()
    }
    
    func catalogDidFailedUpdate()
    {
        self.refreshControl.endRefreshing()
        let alert = UIAlertController(title: "", message: "Произошла ошибка при загрузке каталога", preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel, handler: nil))
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
    func addButtonPressed(sender: AnyObject)
    {
        let button = sender as! UIButton
        let iD = DataManager.sharedInstance.catalogItems[button.tag].iD
        
        if let value = stepperItems[iD]
        {
            if value == 0
            {
                DataManager.sharedInstance.orderDictionary.removeValueForKey(iD)
            }
            else
            {
                DataManager.sharedInstance.orderDictionary[iD] = value
            }

            updateBadge()
        }
    }
    
    func updateBadge()
    {
        var badgeValue = 0
        for (_, value) in DataManager.sharedInstance.orderDictionary
        {
            badgeValue += value
        }
        orderButton.badgeValue = badgeValue > 0 ? "\(badgeValue)" : ""
    }
    
    func stepperPressed(sender: AnyObject)
    {
        let stepper = sender as! SnappingStepper
        let iD = DataManager.sharedInstance.catalogItems[stepper.tag].iD
        let value = Int(stepper.value)
        
        stepperItems[iD] = value
    }
    
    func orderButtonPressed(sender: AnyObject)
    {
        self.performSegueWithIdentifier("orderSegue", sender: nil)
    }
    
    func aboutTabusPressed(sender: AnyObject)
    {
        self.performSegueWithIdentifier("aboutTabusSegue", sender: nil)
    }
    
    // MARK: - UITableView Delegate
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return DataManager.sharedInstance.catalogItems.count
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat
    {
        return 135
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath)
        return cell
    }
    
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath)
    {
        let item = DataManager.sharedInstance.catalogItems[indexPath.row]
        let theCell = cell as! CatalogItemCell
        
        theCell.descriptionLabel.text = item.aDescription
        theCell.pictureImageView.kf_setImageWithURL(NSURL(string: item.picture)!)
        theCell.pictureImageView.kf_showIndicatorWhenLoading = true
        
        theCell.addtocartButton.addTarget(self, action: "addButtonPressed:", forControlEvents: .TouchUpInside)
        theCell.addtocartButton.tag = indexPath.row
        
        theCell.countStepper.addTarget(self, action: "stepperPressed:", forControlEvents: .ValueChanged)
        theCell.countStepper.tag = indexPath.row
        if let value = stepperItems[item.iD]
        {
            theCell.countStepper.value = Double(value)
        }
        else
        {
            theCell.countStepper.value = 0
        }
    }
}
