//
//  Requester.swift
//  efrat
//
//  Created by ifau on 10/12/15.
//  Copyright © 2015 tabus. All rights reserved.
//

import UIKit

class Requester: NSObject
{
    static let sharedInstance = Requester()
    var requestsCount = 0
    
    func performRequest(path path: String, data: NSData?, completion: (NSDictionary?) -> ())
    {
        let session = NSURLSession.sharedSession()
        let str = "http://efrat.azurewebsites.net/api/\(path)"
        let url = NSURL(string: str)!
        let request = NSMutableURLRequest(URL: url)
        
        if data != nil
        {
            request.HTTPBody = data
            request.HTTPMethod = "POST"
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        }
        else
        {
            request.HTTPMethod = "GET"
        }
        
        requestsCount += 1
        UIApplication.sharedApplication().networkActivityIndicatorVisible = true
        
        let task = session.dataTaskWithRequest(request) { [unowned self] (data: NSData?, response: NSURLResponse?, error: NSError?) -> Void in
            
            self.requestsCount -= 1
            UIApplication.sharedApplication().networkActivityIndicatorVisible = self.requestsCount == 0 ? false : true
            
            if error == nil && data != nil
            {
                do
                {
                    let JSON = try NSJSONSerialization.JSONObjectWithData(data!, options: .AllowFragments) as! NSDictionary
                    dispatch_async(dispatch_get_main_queue(), { completion(JSON) })
                }
                catch
                {
                    dispatch_async(dispatch_get_main_queue(), { completion(nil) })
                }
            }
            else
            {
                dispatch_async(dispatch_get_main_queue(), { completion(nil) })
            }
        }
        
        task.resume()
    }
}
