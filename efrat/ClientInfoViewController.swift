//
//  ClientInfoViewController.swift
//  efrat
//
//  Created by ifau on 11/12/15.
//  Copyright © 2015 tabus. All rights reserved.
//

import UIKit

class ClientInfoViewController: UIViewController
{

    @IBOutlet var nameTextField: UITextField!
    @IBOutlet var phoneTextField: UITextField!
    @IBOutlet var cityTextField: UITextField!
    @IBOutlet var streetTextField: UITextField!
    @IBOutlet var houseTextField: UITextField!
    @IBOutlet var apartamentTextField: UITextField!
    @IBOutlet var commentTextField: UITextField!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        nameTextField.text = DataManager.sharedInstance.clientName
        phoneTextField.text =  DataManager.sharedInstance.clientPhone
        cityTextField.text = DataManager.sharedInstance.clientCity
        streetTextField.text = DataManager.sharedInstance.clientStreet
        houseTextField.text = DataManager.sharedInstance.clientHouse
        apartamentTextField.text = DataManager.sharedInstance.clientApartament
        commentTextField.text = DataManager.sharedInstance.clientComment
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "orderDidSend", name: orderWasSuccessfullySendedNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "orderDidFailSend", name: orderWasFailedSendNotification, object: nil)
    }
    
    deinit
    {
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    @IBAction func doneButtonPressed(sender: AnyObject)
    {
        DataManager.sharedInstance.clientName = nameTextField.text
        DataManager.sharedInstance.clientPhone = phoneTextField.text
        //DataManager.sharedInstance.clientCity = cityTextField.text
        DataManager.sharedInstance.clientStreet = streetTextField.text
        DataManager.sharedInstance.clientHouse = houseTextField.text
        DataManager.sharedInstance.clientApartament = apartamentTextField.text
        DataManager.sharedInstance.clientComment = commentTextField.text
        
        if phoneTextField.text?.characters.count < 5
        {
            let alert = UIAlertController(title: "", message: "Укажите номер Вашего телефона", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel, handler: nil))
            self.presentViewController(alert, animated: true, completion: nil)
        }
        else
        {
            DataManager.sharedInstance.sendOrder()
        }
    }
    
    func orderDidSend()
    {
        let alert = UIAlertController(title: "", message: "Ваш заказ успешно отправлен!", preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel, handler: { [unowned self] (action: UIAlertAction) -> Void in
            self.dismissViewControllerAnimated(true, completion: nil)
        }))
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
    func orderDidFailSend()
    {
        let alert = UIAlertController(title: "", message: "Не удалось отправить заказ. Возможно отсутствует интернет-соединение или сервис временно недоступен.", preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel, handler: nil))
        self.presentViewController(alert, animated: true, completion: nil)
    }
//    
//    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?)
//    {
//        self.view.endEditing(true)
//    }
}
